import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        int a = 3;
        int b = 4;
        int c = 5;
        boolean isCoprime = ThreeCoprimeNumbers.areCoprime(a, b, c);
        if (isCoprime) {
            System.out.println(a + ", " + b + ", and " + c + " are coprime.");
        } else {
            System.out.println(a + ", " + b + ", and " + c + " are not coprime.");

        }

        int k = 1000;
        ArrayList<Integer> listOfNumbers;
        listOfNumbers = ArmstrongNumber.findArmstrongNumbers(k);
        for (Integer number : listOfNumbers) {
            System.out.print(number + " ");
        }

        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7};
        int[] sums;
        sums = SubarraysSums.getSubarrays(array);
        for (int sum : sums) {
            System.out.print(sum + " ");
        }
        System.out.println();

        String str1 = "12";
        String str2 = "198";
        System.out.println(SumOfBigIntegers.add(str1, str2));

        int[] unlockingCode;
        unlockingCode = SuperLock.findTheCode(3, 5);
        for (int number : unlockingCode) {
            System.out.print(number + " ");
        }
    }
}