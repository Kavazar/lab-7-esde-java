import java.util.ArrayList;

public class ArmstrongNumber {
    private static boolean isArmstrongNumber(int number) {
        int tempNumber = number;
        int remainder;
        int result = 0;
        while (tempNumber != 0) {
            remainder = tempNumber % 10;
            result += Math.pow(remainder, 3);
            tempNumber /= 10;
        }
        return result == number;
    }

    public static ArrayList<Integer> findArmstrongNumbers(int k) {
        ArrayList<Integer> listOfNumbers = new ArrayList<>();
        for (int i = 1; i <= k; i++) {
            if (isArmstrongNumber(i)) {
                listOfNumbers.add(i);
            }
        }
        return listOfNumbers;
    }
}

