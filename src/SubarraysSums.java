public class SubarraysSums {

    private static int subarraySum(int[] array, int k, int m) {
        int sum = 0;
        if (k > array.length - 1 || m > array.length - 1) {
            throw new IndexOutOfBoundsException();
        }
            for (int i = k; i <= m; i++) {
                sum += array[i];
            }
        return sum;
    }

    public static int[] getSubarrays(int[] array) {
        if (array.length < 7) {
            throw new IllegalArgumentException();
        }
        int[] result = new int[3];
        result[0] = subarraySum(array, 1, 3);
        result[1] = subarraySum(array, 3, 5);
        result[2] = subarraySum(array, 4, 6);
        return result;
    }


}
