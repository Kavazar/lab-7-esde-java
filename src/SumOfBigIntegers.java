public class SumOfBigIntegers {
    private static int carry = 0;

    public static String add(String string1, String string2) {
        if (string1.length() > string2.length()) {
            String temp = string1;
            string1 = string2;
            string2 = temp;
        }

        String result = "";

        string1 = new StringBuilder(string1).reverse().toString();
        string2 = new StringBuilder(string2).reverse().toString();

        result = calculateTillTheEndOf1Number(string1, string2, result);

        result = addRemainingDigits(string1, string2, result);

        if (carry > 0)
            result += (char) (carry + '0');

        result = new StringBuilder(result).reverse().toString();

        return result;
    }

    private static String calculateTillTheEndOf1Number(String string1, String string2, String result) {
        StringBuilder resultBuilder = new StringBuilder(result);
        for (int i = 0; i < string1.length(); i++) {
            int sum = ((string1.charAt(i) - '0') + (string2.charAt(i) - '0') + carry);
            resultBuilder.append((char) (sum % 10 + '0'));

            carry = sum / 10;
        }
        result = resultBuilder.toString();
        return result;
    }

    private static String addRemainingDigits(String string1, String string2, String result) {
        StringBuilder resultBuilder = new StringBuilder(result);
        for (int i = string1.length(); i < string2.length(); i++) {
            int sum = ((string2.charAt(i) - '0') + carry);
            resultBuilder.append((char) (sum % 10 + '0'));
            carry = sum / 10;
        }
        result = resultBuilder.toString();
        return result;
    }
}


