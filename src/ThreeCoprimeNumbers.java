public class ThreeCoprimeNumbers {
    private static int gcd(int number1, int number2) {
        while (number2 != 0) {
            int temp = number2;
            number2 = number1 % number2;
            number1 = temp;
        }
        return number1;
    }

    private static int gcd(int number1, int number2, int number3) {
        return gcd(gcd(number1, number2), number3);
    }

    public static boolean areCoprime(int number1, int number2, int number3) {
        return gcd(number1, number2, number3) == 1;
    }
}
