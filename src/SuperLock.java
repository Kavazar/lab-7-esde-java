public class SuperLock {
    static int[] findTheCode(int first, int second) {
        if (first < 1 || first > 6) {
            System.out.println("Unlocking unreal, incorrect first number");
        }
        if (second < 1 || second > 6) {
            System.out.println("Unlocking unreal, incorrect second number");
        }
        int[] array = new int[10];
        array[0] = first;
        array[1] = second;
        for (int i = 2; i < 10; i++) {
            array[i] = findThird(array[i - 2], array[i - 1]);
        }
        return array;
    }

    private static int findThird(int number1, int number2) {
        int magicNumber = 10;
        int result = magicNumber - number1 - number2;
        if (result < 1 || result > 6) {
            System.out.println("Unlocking unreal");
        }
            return result;
    }
}
